# qr-code-generator

## 简介
> 二维码生成器库。
> 支持按照二维码Model 2标准对所有40个版本（尺寸）和所有4个纠错级别进行编码。
> 输出格式：QR符号的原始模块/像素。

![](screenshot/screenshot.gif)

## 下载安装
```shell
npm install @ohos/qr-code-generator --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

### 1、创建context：
 ```
Context: CanvasRenderingContext2D = new CanvasRenderingContext2D(new RenderingContextSettings(true))
 ```
### 2、通过canvas设置绘制区域：
 ```
Canvas(this.Context).width('80%').height("50%")
 ```
### 3、设置二维码显示内容及容错率：
 ```
qrcode: qrcodegen.QrCode = qrcodegen.QrCode.encodeText("Hello, world!", qrcodegen.QrCode.Ecc.MEDIUM)
 ```
### 4、绘制二维码：
 ```
qrcode.drawCanvas(8, 1, this.Context)
 ```

## 接口说明
1. 二维码符号类:`qrcodegen.QrCode`
   - 获取有效载荷数据：`qrcodegen.QrCode.encodeText（）或qrcodegen.QrCode.encodeBinary（）`
   - 定制片段列表:`qrcodegen.QrCode.encodeSegments（）`
   - 二维码绘制：`qrcodegen.QrCode.drawCanvas（）`
   
2. 数据段类:`qrcodegen.QrSegment`
   - 文本字符串转换为UTF-8字节并编码为字节模式段：`qrcodegen.QrSegment.makeBytes()`
   - 以数字模式编码的给定十进制数字字符串:`qrcodegen.QrSegment.makeNumeric()`
   - 以字母数字模式编码的给定文本字符串：`qrcodegen.QrSegment.makeAlphanumeric()`
   - 一个包含零个或多个段的新可变列表的给定的Unicode文本字符串：`qrcodegen.QrSegment.makeSegments()`
   - 具有给定赋值的扩展通道解释（ECI）指示符：`qrcodegen.QrSegment.makeEci()`
   
3. 二维码符号中的错误纠正级别:`qrcodegen.QrCode.Ecc`

4. 解释段的数据位:`qrcodegen.QrSegment.Mode`

## 约束与限制
在下述版本验证通过：

DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）。

## 目录结构
````
|---- qr-code-generator 
|     |---- entry  # 示例代码文件夹
|     |---- qrcodegen  # qrcodegen库文件夹
|           |---- index.ets  # 对外接口
|           |---- src
|                  |---- main
|                        |---- ets
|                              |---- components
|                                    |---- qrcodegen.ets  #二维码生成实现
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/qr-code-generator/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/qr-code-generator/pulls) 。

## 开源协议
本项目基于 [MIT License](https://gitee.com/openharmony-sig/qr-code-generator/blob/master/LICENSE) ，请自由地享受和参与开源。